package dubbelnull.pictodesigner;

import dubbelnull.pictodesigner.utils.*;
import java.util.ArrayList;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Paper {
    protected int rows;
    protected int columns;
    protected ArrayList<Picto> pictos;

    public Paper() {
        System.out.print("Choose amount of rows you want: ");
        int rows = ScannerTool.readInt();
        
        System.out.print("Choose amount of columns you want: ");
        int columns = ScannerTool.readInt();

        this.rows = rows;
        this.columns = columns;

        pictos = new ArrayList<>();
    }

    public void addPictos() {
        for(int i = 0; i < pictoAmount(); i++) {
            System.out.println("");
            System.out.println("Picto " + (i + 1) + ": ");
            pictos.add(new Picto());
        }
    }

    public void getPictos() {
        for(int i = 0; i < pictos.size(); i++) {
            System.out.println(pictos.get(i));
        }
    }

    public void generatePaperImage() {
        int width = 2100;
        int height = 2970;

        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = bufferedImage.createGraphics();
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1));

        g2d.setColor(Color.white);
        g2d.fillRect(0, 0, width, height);

        int rows = getRows();
        int columns = getColumns();
        int x;
        int y;
        int padding = 0;
        int pictoWidth = 500;
        int pictoHeight = 500;
        int usableWidth = width - (padding * 2);
        int usableHeight = height - (padding * 2);

        for(int i = 0; i < pictoAmount() - 1; i++) {

            for(int row = 0; row < rows; row++) {
                y = (height / rows) * row;

                for(int column = 0; column < columns; column++) {
                    x = (width / columns) * column;

                    g2d.drawImage(BufferedImageTool.resize(pictos.get(i).getBufferedImage(), pictoWidth, pictoHeight), x, y, null);
                    
                }

            }

        }


        g2d.dispose();

        File file = new File("/data/papers/output.png");
        try {
            ImageIO.write(bufferedImage, "png", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    public int getColumns() {
        return columns;
    }

    public int getRows() {
        return rows;
    }

    public int pictoAmount() {
        return getRows() + getColumns();
    }
    
}

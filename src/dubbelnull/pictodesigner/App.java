package dubbelnull.pictodesigner;

import dubbelnull.pictodesigner.utils.*;

public class App {
    public static void main(String[] args) throws Exception {
        menu();
    }

    public static void menu() {
        System.out.println("");
        System.out.println("Enter the number that corresponds to your desired action.");
        System.out.println("To get back to this menu, insert 0 (zero) at any point in this program.");
        System.out.println("===========================");
        System.out.println("1) Make new paper");
        System.out.println("2) Settings");
        System.out.println("3) Quit");
        System.out.println("===========================");

        int menuChoice = ScannerTool.readInt();
        NavigationTool.menu(menuChoice);

        switch(menuChoice) {
            case 1:
                newPaper();
                break;
            case 2:
                settings();
                break;
            case 3:
                System.exit(0);
                break;
        }
    }

    public static void newPaper() {
        Paper paper = new Paper();
        paper.addPictos();
        paper.generatePaperImage();
    }

    public static void settings() {
        System.out.println("");
        System.out.println("Choose number to change setting: ");
        System.out.println("1) Paper aspect ratio (current: square root of 2, A4 paper)");
        System.out.println("2) Go back");
        int menuChoice = ScannerTool.readInt();
        NavigationTool.menu(menuChoice);

        switch(menuChoice) {
            case 1:
                System.out.println("Desire aspect ratio (format: 1:1 = square): ");
                String aspectRatio = ScannerTool.readString();

            case 2:
                menu();
        }
    }
}

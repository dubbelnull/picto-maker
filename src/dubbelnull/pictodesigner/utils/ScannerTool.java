package dubbelnull.pictodesigner.utils;

import java.util.Scanner;

public class ScannerTool {

    public static int readInt() {
        Scanner scanner = new Scanner(System.in); 
        int result = Integer.parseInt(scanner.nextLine());
        return result;
    }
    
    public static String readString() {
        Scanner scanner = new Scanner(System.in); 
        String result = scanner.nextLine();
        return result;
    }

    public static char readChar() {
        Scanner scanner = new Scanner(System.in); 
        char result = scanner.next().charAt(0);
        return result;
    }
    
}

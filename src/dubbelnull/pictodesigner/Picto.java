package dubbelnull.pictodesigner;

import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import dubbelnull.pictodesigner.utils.*;

public class Picto {
    protected BufferedImage image;
    protected String imageUrl;

    public Picto() {
        init();
    }

    public void init() {
        System.out.print("link to image: ");
        this.imageUrl = ScannerTool.readString();
        NavigationTool.menu(this.imageUrl);
        try {
            this.image = ImageIO.read(new File(this.imageUrl));
        } catch (IOException e) {
            System.err.println("The URL provided did not match an existing image. Retrying...");
            init();
        }
    }

    @Override
    public String toString() {
        return image + "";
    }

    public BufferedImage getBufferedImage() {
        return image;
    }

}
